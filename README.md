## Application Description

This application is dockerized and can be executed on port 8005 on the local host.

## API Documentation

The API documentation is available as a Swagger package at the following address: [http://localhost:8005/api/documentation](http://localhost:8005/api/documentation).

## Software Implementation

The software is implemented using Laravel and serves as a REST API.
